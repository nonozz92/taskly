import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../widgets/drawer.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  Future<Map<String, dynamic>?> getUserData(String userId) async {
    try {
      DocumentSnapshot userDoc = await FirebaseFirestore.instance
          .collection('users')
          .doc(userId)
          .get();
      if (userDoc.exists) {
        return userDoc.data() as Map<String, dynamic>?;
      }
    } catch (e) {
      print("$e");
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Profil'),
      ),
      drawer: const AppDrawer(currentPage: 'profile'),
      body: user != null
          ? FutureBuilder<Map<String, dynamic>?>(
              future: getUserData(user.uid),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                }
                if (snapshot.hasError) {
                  return Center(child: Text('Erreur: ${snapshot.error}'));
                }
                if (!snapshot.hasData || snapshot.data == null) {
                  return const Center(
                      child: Text('Aucune donnée utilisateur trouvée'));
                }

                Map<String, dynamic> userData = snapshot.data!;

                return Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: CircleAvatar(
                          radius: 50,
                          backgroundColor: Colors.blue,
                          child: Text(
                            userData['prenom'] != null
                                ? userData['prenom'][0].toUpperCase()
                                : '?',
                            style: const TextStyle(fontSize: 40),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Expanded(
                        child: ListView(
                          children: [
                            ListTile(
                              leading: const Icon(Icons.person),
                              title: Text(
                                  'Nom : ${userData['nom'] ?? "Non renseigné"}'),
                            ),
                            ListTile(
                              leading: const Icon(Icons.person_outline),
                              title: Text(
                                  'Prénom : ${userData['prenom'] ?? "Non renseigné"}'),
                            ),
                            ListTile(
                              leading: const Icon(Icons.cake),
                              title: Text(
                                  'Date de Naissance : ${userData['dateDeNaissance'] ?? "Non renseigné"}'),
                            ),
                            ListTile(
                              leading: const Icon(Icons.email),
                              title: Text(
                                  'Email : ${user.email ?? "Non connecté"}'),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            )
          : const Center(child: Text('Non connecté')),
    );
  }
}
