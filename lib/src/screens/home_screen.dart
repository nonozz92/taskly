import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../widgets/drawer.dart';
import 'addTask_screen.dart';
import 'detailsTache_screen.dart';
import 'editTask_screen.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';

String formatDateTime(DateTime dateTime) {
  return DateFormat('yyyy-MM-dd').format(dateTime);
}

String formatDateTime2(DateTime dateTime) {
  return DateFormat('dd/MM/yyyy').format(dateTime);
}

Stream<List<Task>> getUserTasks() {
  var user = FirebaseAuth.instance.currentUser;
  if (user != null) {
    return FirebaseFirestore.instance
        .collection('tasks')
        .where('userId', isEqualTo: user.uid)
        .orderBy('dueDate')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) => Task.fromFirestore(doc)).toList();
    });
  } else {
    return Stream.value([]);
  }
}

class Task {
  String id;
  String title;
  String description;
  DateTime dueDate;
  DateTime creationDate;

  Task({
    required this.id,
    required this.title,
    required this.description,
    required this.dueDate,
    required this.creationDate,
  });

  factory Task.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
    return Task(
      id: doc.id,
      title: data['title'] ?? '',
      description: data['description'] ?? '',
      dueDate: (data['dueDate'] as Timestamp).toDate(),
      creationDate: (data['creationDate'] as Timestamp).toDate(),
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      'title': title,
      'description': description,
      'dueDate': Timestamp.fromDate(dueDate),
      'creationDate': Timestamp.fromDate(creationDate),
    };
  }

  @override
  String toString() {
    return 'Task{id: $id, title: $title, dueDate: $dueDate}';
  }
}

class TaskCard extends StatefulWidget {
  final Task task;
  final bool showActions;

  const TaskCard({Key? key, required this.task, this.showActions = true})
      : super(key: key);

  @override
  _TaskCardState createState() => _TaskCardState();
}

class _TaskCardState extends State<TaskCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: ListTile(
          title: Text(widget.task.title),
          subtitle: Text('Échéance: ${formatDateTime2(widget.task.dueDate)}'),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TaskDetailsPage(task: widget.task),
              ),
            );
          },
          trailing: widget.showActions
              ? Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                      icon: const Icon(Icons.edit, color: Colors.blue),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                EditTaskPage(task: widget.task),
                          ),
                        );
                      },
                    ),
                    IconButton(
                      icon: const Icon(Icons.delete, color: Colors.red),
                      onPressed: () async {
                        bool shouldDelete = await showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: const Text('Supprimer la tâche'),
                                  content: const Text(
                                      'Êtes-vous sûr de vouloir supprimer cette tâche?'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(false),
                                      child: const Text('Annuler'),
                                    ),
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(true),
                                      child: const Text('Supprimer'),
                                    ),
                                  ],
                                );
                              },
                            ) ??
                            false;

                        if (shouldDelete && mounted) {
                          await FirebaseFirestore.instance
                              .collection('tasks')
                              .doc(widget.task.id)
                              .delete();
                        }
                      },
                    ),
                  ],
                )
              : null,
        ),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  final List<Widget> _pages = [
    TaskListView(),
    CalendarView(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mes Tâches'),
      ),
      drawer: const AppDrawer(currentPage: 'task_list'),
      body: Center(
        child: _pages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Tâches',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            label: 'Calendrier',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromARGB(255, 0, 0, 0),
        onTap: _onItemTapped,
      ),
      floatingActionButton: _selectedIndex == 0
          ? FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddTaskPage()),
                );
              },
              tooltip: 'Ajouter une tâche',
              child: const Icon(Icons.add),
            )
          : null,
    );
  }
}

class TaskListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Task>>(
      stream: getUserTasks(),
      builder: (context, AsyncSnapshot<List<Task>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        }
        if (snapshot.error != null) {
          return const Center(child: Text('Une erreur est survenue'));
        }
        final tasks = snapshot.data ?? [];
        return ListView.builder(
          itemCount: tasks.length,
          itemBuilder: (context, index) {
            return TaskCard(task: tasks[index]);
          },
        );
      },
    );
  }
}

class CalendarView extends StatefulWidget {
  @override
  _CalendarViewState createState() => _CalendarViewState();
}

class _CalendarViewState extends State<CalendarView> {
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  Map<DateTime, List<Task>> tasksByDay = {};
  List<Task> _tasksForSelectedDay = [];

  @override
  void initState() {
    super.initState();
    _retrieveTasks();
  }

  void _retrieveTasks() {
    getUserTasks().listen((List<Task> tasksList) {
      if (mounted) {
        setState(() {
          tasksByDay = {};
          for (var task in tasksList) {
            DateTime taskDateUTC = DateTime.utc(
                task.dueDate.year, task.dueDate.month, task.dueDate.day);
            tasksByDay[taskDateUTC] ??= [];
            tasksByDay[taskDateUTC]!.add(task);
          }
        });
      }
    }, onError: (error) {
      if (mounted) {
        print('$error');
      }
    });
  }

  void _getTasksForDay(DateTime date) {
    DateTime keyDate = DateTime.utc(date.year, date.month, date.day);
    var tasksForTheDay = tasksByDay[keyDate] ?? [];
    if (mounted) {
      setState(() {
        _tasksForSelectedDay = tasksForTheDay;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TableCalendar<Task>(
          eventLoader: (date) {
            return tasksByDay[date] ?? [];
          },
          firstDay: DateTime.utc(2010, 10, 16),
          lastDay: DateTime.utc(2030, 3, 14),
          focusedDay: _focusedDay,
          calendarFormat: _calendarFormat,
          selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
          onDaySelected: (selectedDay, focusedDay) {
            if (!isSameDay(_selectedDay, selectedDay)) {
              setState(() {
                _selectedDay = selectedDay;
                _focusedDay = focusedDay;
                print('Jour sélectionné: $_selectedDay');
              });
              _getTasksForDay(selectedDay);
            }
          },
          onFormatChanged: (format) {
            if (_calendarFormat != format) {
              setState(() {
                _calendarFormat = format;
              });
            }
          },
          onPageChanged: (focusedDay) {
            _focusedDay = focusedDay;
          },
          calendarBuilders: CalendarBuilders(
            markerBuilder: (context, date, events) {
              if (events.isNotEmpty) {
                return Positioned(
                  right: 1,
                  bottom: 1,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.circle,
                    ),
                    width: 7,
                    height: 7,
                  ),
                );
              }
              return null;
            },
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: _tasksForSelectedDay.length,
            itemBuilder: (context, index) {
              Task task = _tasksForSelectedDay[index];
              return TaskCard(task: task, showActions: false);
            },
          ),
        ),
      ],
    );
  }
}
