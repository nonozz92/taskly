import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  SignUpPageState createState() => SignUpPageState();
}

class SignUpPageState extends State<SignUpPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordController = TextEditingController();

  String? _email,
      _password,
      _nom,
      _prenom,
      _dateDeNaissance,
      _passwordConfirmation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Inscription'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  buildTextField('Email', (input) {
                    if (input?.isEmpty ?? true) {
                      return 'Veuillez saisir un email';
                    }
                    return null;
                  }, (input) => _email = input),
                  const SizedBox(height: 20),
                  buildTextField('Nom', (input) {
                    if (input?.isEmpty ?? true) {
                      return 'Veuillez saisir votre nom';
                    }
                    return null;
                  }, (input) => _nom = input),
                  const SizedBox(height: 20),
                  buildTextField('Prénom', (input) {
                    if (input?.isEmpty ?? true) {
                      return 'Veuillez saisir votre prénom';
                    }
                    return null;
                  }, (input) => _prenom = input),
                  const SizedBox(height: 20),
                  buildTextField('Date de naissance (JJ/MM/AAAA)', null,
                      (input) => _dateDeNaissance = input),
                  const SizedBox(height: 20),
                  buildTextField('Mot de passe', (input) {
                    if ((input?.length ?? 0) < 6) {
                      return 'Votre mot de passe doit contenir au moins 6 caractères';
                    }
                    return null;
                  }, (input) => _password = input,
                      obscureText: true, controller: _passwordController),
                  const SizedBox(height: 20),
                  buildTextField('Confirmer le mot de passe', (input) {
                    if ((input?.isEmpty ?? true) ||
                        (_passwordController.text != input)) {
                      return 'Les mots de passe ne correspondent pas';
                    }
                    return null;
                  }, (input) => _passwordConfirmation = input,
                      obscureText: true),
                  const SizedBox(height: 20),
                  ConstrainedBox(
                    constraints: const BoxConstraints.tightFor(width: 300),
                    child: ElevatedButton(
                      onPressed: signUp,
                      child: const Text('S\'inscrire'),
                    ),
                  ),
                  const SizedBox(height: 20),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text(
                      "Vous avez déjà un compte ? Connectez-vous",
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildTextField(String labelText, String? Function(String?)? validator,
      void Function(String?)? onSaved,
      {bool obscureText = false, TextEditingController? controller}) {
    return ConstrainedBox(
      constraints: const BoxConstraints.tightFor(width: 300),
      child: TextFormField(
        controller: controller,
        validator: validator,
        onSaved: onSaved,
        decoration: InputDecoration(labelText: labelText),
        obscureText: obscureText,
      ),
    );
  }

  Future<void> signUp() async {
    final formState = _formKey.currentState;
    final scaffoldMessenger = ScaffoldMessenger.of(context);

    if (formState != null && formState.validate()) {
      formState.save();

      if (_password != _passwordConfirmation) {
        scaffoldMessenger.showSnackBar(const SnackBar(
            content: Text('Les mots de passe ne correspondent pas.')));
        return;
      }

      try {
        UserCredential userCredential =
            await _auth.createUserWithEmailAndPassword(
                email: _email ?? '', password: _password ?? '');
        FirebaseFirestore.instance
            .collection('users')
            .doc(userCredential.user?.uid)
            .set({
          'nom': _nom,
          'prenom': _prenom,
          'dateDeNaissance': _dateDeNaissance,
          'email': _email,
        });
        scaffoldMessenger.showSnackBar(
            const SnackBar(content: Text('Compte créé avec succès !')));
        Future.delayed(const Duration(seconds: 2), () {
          Navigator.of(context).pushReplacementNamed('/login');
        });
      } on FirebaseAuthException catch (e) {
        scaffoldMessenger
            .showSnackBar(SnackBar(content: Text('Erreur: ${e.message}')));
      } catch (e) {
        debugPrint(e.toString());
      }
    }
  }
}
